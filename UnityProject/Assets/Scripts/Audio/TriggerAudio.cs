﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAudio : MonoBehaviour
{
    [System.Serializable]
    public class TriggerSong
    {
        public string tag;
        [Tooltip("Name of the audio on audiomanager. Leave it empty to ignore this tag")]
        public string audioName;
    }

    [SerializeField]
    bool _onTriggerEnter;
    public bool onTriggerEnter { get { return _onTriggerEnter; } set { _onTriggerEnter = value; } }

    [SerializeField]
    bool _onTriggerExit;
    public bool onTriggerExit { get { return _onTriggerExit; } set { _onTriggerExit = value; } }

    [SerializeField]
    bool _onCollisionEnter;
    public bool onCollisionEnter { get { return _onCollisionEnter; } set { _onCollisionEnter = value; } }

    [SerializeField]
    bool _onCollisionExit;
    public bool onCollisionExit { get { return _onCollisionExit; } set { _onCollisionExit = value; } }

    [Tooltip("Play on a soundcomponent here on the object collided with")]
    public bool playOnSelf;

    public bool playSourceSound = false;

    [Tooltip("If null will ignore tags that not appear on the songs list. Else will play whenever a song isn't found on the tag list.")]
    public string audioNameDefault = "";

    public List<TriggerSong> sounds;

    public float minVelocity;

    SoundComponent mineSound;

    private void OnTriggerEnter(Collider other)
    {
        if (onTriggerEnter)
        {
            if (minVelocity != 0)
            {
                Rigidbody bodyOther = other.GetComponent<Rigidbody>(), BodyThis = GetComponent<Rigidbody>();
                if (bodyOther.velocity.magnitude > minVelocity)
                    Trigger(other.gameObject);
                else if (BodyThis.velocity.magnitude > minVelocity)
                    Trigger(other.gameObject);
            }
            else
                Trigger(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (onTriggerExit)
        {
            if (minVelocity != 0)
            {
                Rigidbody bodyOther = other.GetComponent<Rigidbody>(), BodyThis = GetComponent<Rigidbody>();
                if (bodyOther.velocity.magnitude > minVelocity)
                    Trigger(other.gameObject);
                else if (BodyThis.velocity.magnitude > minVelocity)
                    Trigger(other.gameObject);
            }
            else
                Trigger(other.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (onCollisionEnter)
        {
            if (minVelocity == 0 || collision.relativeVelocity.magnitude > minVelocity)
            {
                Trigger(collision.collider.gameObject);
            }

        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (onCollisionExit)
        {
            if (minVelocity == 0 || collision.relativeVelocity.magnitude > minVelocity)
            {
                Trigger(collision.collider.gameObject);
            }

        }
    }


    public void Trigger(GameObject g)
    {
        if (playSourceSound)
            GetComponent<SoundComponent>().Play();
        SoundComponent sound;
        if (playOnSelf)
            sound = GetComponent<SoundComponent>();
        else
            sound = g.GetComponent<SoundComponent>();
        if (sound == null)
            return;

        int soundID = -1;
        for (int i = 0; i < sounds.Count; i++)
        {
            if (g.CompareTag(sounds[i].tag))
            {
                soundID = i;
                break;
            }
        }
        if (soundID == -1) // Se não tem um som relacionado a tag do objeto, tenta tocar o som default
        {
            if (audioNameDefault == "")
                return;

            sound.Play(audioNameDefault, true);

        }
        else if (sounds[soundID].audioName == "") // if theres an tag but there is no audio, then ignore this class
            return;
        else
            sound.Play(sounds[soundID].audioName, true);




    }
}
