﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class CalibrationUI : MonoBehaviour
{

    AccelerationArduino acceleration;
    public TMPro.TextMeshProUGUI text;
    bool calibrating = false;
    public Button calibrationButton;
    public Button[] deactivateButton;

    // Use this for initialization
    void Start()
    {
        acceleration = ArduinoInterface.Instance.GetComponent<AccelerationArduino>();
        God.Events.EventManager.Instance.AddListener<ForwardCalibrationDone>(forwardCalibrationDone);
    }

    private void forwardCalibrationDone(ForwardCalibrationDone e)
    {
        calibrating = false;
        for (int i = 0; i < deactivateButton.Length; i++)
            deactivateButton[i].interactable = true;
        calibrationButton.interactable = true;

    }

    // Update is called once per frame
    void Update()
    {
        text.text = acceleration.stringDebug;
    }

    public void Calibrate()
    {
        if(!ArduinoInterface.Instance.connected)
            ArduinoInterface.Instance.ConnectToArduino();

        if (!calibrating && ArduinoInterface.Instance.connected)
        {
            calibrating = true;
            acceleration.StartCalibration();
            calibrationButton.interactable = false;
            for (int i = 0; i < deactivateButton.Length; i++)
                deactivateButton[i].interactable = false;

        }
    }


}
