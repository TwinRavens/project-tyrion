﻿using God.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// the game manager is actually controlling more than the gameplay loop, it is controlling the camera as well
public class GameManager : MonoBehaviour
{
	public class GameStateChanged : GameEvent
	{
		public GameState newState;
		public GameStateChanged(GameState state)
		{
			newState = state;
		}
	}

	public class GameLaunchReady : GameEvent
	{

	}

	public class ThrowBallEvent : GameEvent
	{
		public Vector3 force;
		public ThrowBallEvent(Vector3 vector3)
		{
			force = vector3;
		}
	}

	[System.Serializable]
	public enum GameState
	{
		view,
		readyForLaunch,
		afterLaunch,
		endGame
	}

	//[System.Serializable]



	[Header("Bolim")]
	public Color bolimColor = Color.white;
	[Header("Team 1")]
	public Color team1Color = Color.blue;
	[Header("Team 2")]
	public Color team2Color = Color.yellow;
	[Space(15)]
	[Header("Gameplay")]
	[Tooltip("Prefab base for balls.")]
	public GameObject ballPrefab;
	[Tooltip("Reference of the bolim.")]
	public GameObject bolim;
	public Ball bolimBall;
	
	[Tooltip("List of balls in match.")]
	public List<Ball> balls;
	public int scoreForVictory = 5;
	public int ballsForTeam = 5;
	public List<Round> scoreboard;
	public int roundCounter = 0;
	public bool canLaunch = false;
	[System.NonSerialized]
	public bool hasABolim = false;
	public bool canInteract = true;
	public GameState gameState;
	//[System.NonSerialized]
	private DynamicCamera dCam;
	[System.NonSerialized]
	public bool teamCounter = true;
	public Scoreboard scoreboardObject;
	public Transform launchPoint;
	public Transform viewLaunchPoint;

	public Player[] player = new Player[2];

	private Transform ballTarget;
	private Vector3 releasePoint;
	private bool calibrating = true;
	private bool feedbackEnable = true;

	//Using for Coroutines
	private bool isViewing = false;
	private bool endGame = false;

	private EventManager eventManager;
	private void Awake()
	{
		eventManager = EventManager.Instance;

		player[0].color = team1Color;
		player[1].color = team2Color;

		dCam = Camera.main.GetComponent<DynamicCamera>();

		scoreboardObject.gameObject.SetActive(false);
	}

	public void StartGame()
	{
		scoreboardObject.gameObject.SetActive(true);

		gameState = GameState.view;
		eventManager.TriggerEvent(new GameStateChanged(gameState));

		scoreboard = new List<Round>();
		StartCoroutine(ReturnLaunchPoint(3f));
		eventManager.AddListener<ThrowBallEvent>(OnThrowBall);

	}
	private void OnThrowBall(ThrowBallEvent e)
	{
		ThrowBall(e.force);
	}

	private void NewRound()
	{
		if (roundCounter > 0)
		{
			scoreboardObject.round.text = (roundCounter + 1).ToString();

			//Clear list of balls
			for (int i = balls.Count - 1; i >= 0; i--)
				Destroy(balls[i].gameObject);
			for (int i = balls.Count - 1; i >= 0; i--)
				balls.RemoveAt(i);
			scoreboardObject.Reset();
			Destroy(bolim);
			hasABolim = false;
			bolim = null;
			canLaunch = true;
		}
	}

	private void ChoosingLaunchPoint() // does this name make sense regarding what is done inside it?
	{
		if (balls.Count >= (ballsForTeam * 2))
		{
			roundCounter++;
			for (int i = 0; i < player.Length; i++)
				player[i].ResetRound();
			EndOfRound();
		}
		else
		{
			//Launching
			canLaunch = true;

		}
		//Debug.Log("CHEGOU NO FINAL DO CHOOSSING!");
		//Probably here is where the ball is ready to be launched, right?
		eventManager.TriggerEvent(new GameLaunchReady());
	}


	private void randomThrow()
	{
		ThrowBall(Vector3.up * Random.Range(250f, 325f) + Vector3.forward * Random.Range(250f, 325f));
	}
	public void ThrowBall(Vector3 vector3)
	{
		if (!canInteract && !canLaunch) // These two variables could be considered in a game state 
		{
			Debug.LogError("Can't interact or launch right now");
			return;
		}
		releasePoint = launchPoint.transform.position;  //teste
		Rigidbody rb;
		if (!hasABolim)
		{
			bolim = Instantiate(ballPrefab, releasePoint, new Quaternion(), transform);
			bolim.name = "Bolim";
			bolim.transform.GetChild(0).gameObject.SetActive(false);
			rb = bolim.GetComponent<Rigidbody>();
			bolimBall = bolim.GetComponent<Ball>();
			bolimBall.bolim = true; //rb.isKinematic = true;
			rb.mass = 0.49f;
			rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
			//Reference for animation
			ballTarget = bolim.transform;
			hasABolim = true;
		}
		else
		{

			GameObject GO = Instantiate(ballPrefab, releasePoint, new Quaternion(), transform);
			Ball ball = GO.GetComponent<Ball>();
			rb = GO.GetComponent<Rigidbody>();
			ball.bolim = false;
			ball.SetTeam((teamCounter ? 1 : 2), player[(teamCounter ? 0 : 1)].color);
			player[(teamCounter ? 0 : 1)].ballCounter++;
			teamCounter = !teamCounter;
			//rb.isKinematic = true;
			GO.transform.localScale = Vector3.one * 0.35f;
			balls.Add(ball);
			GO.name = "Ball " + ((balls.Count + balls.Count % 2) / 2).ToString() + " Team " + (!teamCounter ? "1" : "2");
			//Reference for animation
			ballTarget = GO.transform;
		}

		rb.AddForce(vector3, ForceMode.VelocityChange);
		canLaunch = false;
		canInteract = false;
		gameState = GameState.afterLaunch;
		eventManager.TriggerEvent(new GameStateChanged(gameState));
		StartCoroutine(Follow());

	}

	private void EndOfRound()
	{
		//Applying the score of the last round
		int[] value = CurrentScore();
		player[value[0] - 1].score += value[1];
		scoreboard.Add(new Round(value[0], value[1]));
		scoreboardObject.SetScores();

		if (Player.Winner(player, scoreForVictory))
		{
			//Debug.Log("FINAL DO ROUND - END GAME");
			canInteract = false;
			gameState = GameState.endGame;
			endGame = true;
		}
		else
		{
			//Debug.Log("FINAL DO ROUND - NEW ROUND");
			NewRound();
		}
	}

	private void CalcDistance(bool enable)
	{
		for (int i = 0; i < balls.Count; i++)
		{
			balls[i].distance = Vector3.Distance(
			new Vector3(balls[i].transform.position.x, 0f, balls[i].transform.position.z),
			new Vector3(bolim.transform.position.x, 0f, bolim.transform.position.z));
			if (enable)
				balls[i].EnableDistance(true);
		}
		balls.Sort((p1, p2) => p1.distance.CompareTo(p2.distance));
	} 

	private float SortByDistance(Ball b1, Ball b2)
	{
		return b1.distance.CompareTo(b2.distance);
	}

	/// <summary>
	/// Calculate the score.
	/// </summary>
	/// <returns>Returns the score.</returns>
	private int Score()
	{
		int score = 0;
		if (balls.Count > 0)
			for (int i = 0; i < balls.Count; i++)
				if (balls[0].team == balls[i].team)
					score += 1;
				else
					break;
		else
			return 0;
		return score;
	}

	/// <summary>
	/// Returns the score of the round.
	/// </summary>
	/// <returns>value[0] == team && value[1] == score</returns>
	public int[] CurrentScore()
	{
		int[] value = new int[2];
		value[0] = value[1] = 0;
		if (balls.Count > 0)
			value[0] = balls[0].team;
		else
			return value;

		value[1] = Score();

		return value;
	}

	public bool IsMoving()
	{
		for (int i = 0; i < balls.Count; i++)
		{
			if (balls[i].moving && balls[i].inGame)
				return true;
		}
		if(bolimBall.moving && bolimBall.inGame )
			return true;

		bool view;
		if (gameState != GameState.view)
			view = false;
		else
			view = true;
		//isViewing = false;

		if (gameState != GameState.endGame)
			gameState = GameState.view;
		if (!view)
			eventManager.TriggerEvent(new GameStateChanged(gameState));

		return false;
	}

	private void LateUpdate()
	{
		//Debug.Log(isViewing);
		switch (gameState) // 
		{
			/////// VIEW ////////
			case GameState.view:
				if (hasABolim)
				{
					if (!isViewing) // I think that his shouldn't be here in a update based function, you created an variable just so it does not repeat. Maybe it should be in event function or be called when the state is called
					{
						isViewing = true;
						//Debug.Log("To entrando!!!");
						StartCoroutine(DynamicCamera());
					}
				}
				else if (!canLaunch)
				{
					gameState = GameState.readyForLaunch;
					eventManager.TriggerEvent(new GameStateChanged(gameState));
					eventManager.TriggerEvent(new GameLaunchReady());

				}
				CalcDistance(feedbackEnable);
				break;

			/////// LAUNCH ////////
			case GameState.readyForLaunch:

				// This here was ok when testing ;D

				break;

			/////// AFTERLAUNCH ////////
			case GameState.afterLaunch:
				CalcDistance(feedbackEnable);
				break;

			case GameState.endGame:
				if (!IsMoving())
					if (endGame)
					{
						EndGame();
						endGame = false;
					}
				break;
			default:
				break;
		}

		if (Input.GetKeyDown(KeyCode.Return))
		{
			SetFeedBackDistance();
		}
		if (Input.GetKeyDown(KeyCode.R))
			RestartGame();

	}

	private void SetFeedBackDistance()
	{
		feedbackEnable = !feedbackEnable;
		if (!feedbackEnable)
		{
			for (int i = 0; i < balls.Count; i++)
			{
				balls[i].EnableDistance(false);
			}
		}
	}

	private IEnumerator DynamicCamera()
	{
		//Debug.Log("Iniciando visualização!");
		List<Transform> list = new List<Transform>();
		for (int i = 0; i < balls.Count; i++)
			if (balls[i].inGame)
				list.Add(balls[i].transform);
		list.Add(bolim.transform);
		dCam.enabled = true;
		dCam.trackList = list;
		yield return new WaitForSeconds(3f);
		dCam.enabled = false;

		if (gameState != GameState.endGame)
			gameState = GameState.readyForLaunch;
		eventManager.TriggerEvent(new GameStateChanged(gameState));

		//Debug.Log("Terminando visualização!");

		StartCoroutine(ReturnLaunchPoint(1f));
	}

	private IEnumerator ReturnLaunchPoint(float time)
	{
		//Debug.Log("Entrei no ReturnLaunchPoint");
		//Return for launch point
		Vector3 position = Camera.main.transform.position;
		Quaternion rotation = Camera.main.transform.rotation;
		float counterLerp = 0f;


		while (counterLerp < time)
		{
			counterLerp += Time.deltaTime;
			Camera.main.transform.position = Vector3.Lerp(position, viewLaunchPoint.position, counterLerp / time);
			Camera.main.transform.rotation = Quaternion.Lerp(rotation, viewLaunchPoint.rotation, counterLerp / time);
			yield return null;
		}
		if (gameState != GameState.endGame)
		{
			canInteract = true;

			ChoosingLaunchPoint();
		}
	}

	private void EndGame()
	{
		if (player[0].score > player[1].score)
			scoreboardObject.EndGame(true);
		else
			scoreboardObject.EndGame(false);
	}

	private IEnumerator Follow()
	{
		yield return new WaitForSeconds(0.2f); // wait a little bit before following the ball.

		float counterLerp = 0f;
		Ball ball = ballTarget.GetComponent<Ball>();
		float timer = 0f;

		while (timer < 3f || IsMoving() || (ball.moving && ball.inGame))
		{
			timer += Time.deltaTime;
			counterLerp += Input.GetAxis("Vertical") * Time.deltaTime;
			Camera.main.transform.position = Vector3.Lerp(
				new Vector3(ballTarget.position.x,
				ballTarget.position.y + 5f,
				ballTarget.position.z - 10f),
				new Vector3(ballTarget.position.x,
				ballTarget.position.y + 2.5f,
				ballTarget.position.z - 3f),
				counterLerp);
			Camera.main.transform.LookAt(ballTarget);
			yield return null;
		}

		isViewing = false;

		if (ball.bolim)
		{
			if (!ball.inGame)
			{
				hasABolim = false;
				bolim = null;
				if (gameState != GameState.endGame)
					gameState = GameState.readyForLaunch;
				eventManager.TriggerEvent(new GameStateChanged(gameState));

				StartCoroutine(ReturnLaunchPoint(1f));
			}
		}
		else
		{
			if (gameState != GameState.endGame)
				gameState = GameState.view;
			eventManager.TriggerEvent(new GameStateChanged(gameState));
		}
	}

	public void RestartGame()
	{
		SceneManager.LoadScene(0);
	}
}