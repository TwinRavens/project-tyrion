﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeThrow : MonoBehaviour
{

    [InspectorReadOnly, SerializeField]
    private bool canThrow = false;
    public GameObject model;
    public Material materialFeedback;

    [MinMaxRange(0,1000)]
    public MinMaxRange rangeForce;
    [InspectorReadOnly, SerializeField]
    private float currentForce;

    private Vector3 rotation;
    public float torque = 1f;
    public float forceVel = 1f;
    God.Events.EventManager eventManager;
    public Color feedbackColorMin;
    public Color feedbackColorMax;
    // Use this for initialization
    void Awake()
    {
        eventManager = God.Events.EventManager.Instance;
        eventManager.AddListener<GameManager.GameLaunchReady>(CanThrow);
        eventManager.AddListener<GameManager.GameStateChanged>(GameStateChanged);
        model.SetActive(false);
    }

    private void GameStateChanged(GameManager.GameStateChanged e)
    {
        if (e.newState != GameManager.GameState.readyForLaunch)
        {
            model.SetActive(false);
            canThrow = false;
        }
    }

    public void CanThrow(GameManager.GameLaunchReady gameLaunchReady)
    {
        canThrow = true;
        model.SetActive(true);
        transform.rotation = Quaternion.identity;
        rotation = Vector3.zero;
        currentForce = rangeForce.rangeStart;
    }

    // Update is called once per frame
    void Update()
    {
        if (canThrow)
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                rotation.y += torque * Time.deltaTime;
            }
            else if (Input.GetAxis("Horizontal") < 0)
            {
                rotation.y -= torque * Time.deltaTime;
            }

            if (Input.GetAxis("Vertical") > 0)
            {
                rotation.x += torque * Time.deltaTime;
            }
            else if (Input.GetAxis("Vertical") < 0)
            {
                rotation.x -= torque * Time.deltaTime;
            }
            transform.rotation = Quaternion.Euler(rotation);
            if (Input.GetButton("Throw"))
            {
                currentForce += forceVel * Time.deltaTime;
                if (currentForce >= rangeForce.rangeEnd)
                {
                    forceVel *= -1;
                    currentForce = rangeForce.rangeEnd;
                }
                if (currentForce <= rangeForce.rangeStart)
                {
                    forceVel *= -1;
                    currentForce = rangeForce.rangeStart;
                }
                materialFeedback.color = Color.Lerp(feedbackColorMin, feedbackColorMax,
                    (currentForce - rangeForce.rangeStart) / (rangeForce.rangeEnd - rangeForce.rangeStart));
            }
            if (Input.GetButtonUp("Throw"))
            {
                eventManager.TriggerEvent(new GameManager.ThrowBallEvent(transform.forward * currentForce));
                canThrow = false;
            }
        }
    }
}
