﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Xml;

public class ContentListDisplayer : MonoBehaviour
{
    #region Enum
    public enum TypeOfDisplay
    {
        OneColumn,
        OneRow,
        InvertColumn, // Need some work
        InvertRow//,
                 // FillAll
    }
    public enum typeContent
    {
        Image,
        Text,
        Blank,
        None
    }
    public enum OrganizeType
    {
        nothing,
        continueColumn,
        continueRow,
        continueRowColumn,
        jumpRow,
        jumpColumn
    }
    #endregion
    #region Classes
    public interface Content
    {
        ContentHolder Parent { get; set; }
        typeContent getType();
    }

    /// <summary>
    /// Class that holds the informatin to display the content
    /// </summary>
    [Serializable]
    public class ContentHolder
    {
        public ContentHolder(typeContent type)
        {
            this.type = type;
            instanceContent();
            name = "Content";
            color = Color.white;
            width = 200;
            height = 200;
            update = true;
        }

        #region Stylus
        /// <summary>
        /// Tag to use to search for the desire style when using the Stylus mode
        /// </summary>
        public string tagStylus;
        #endregion

        #region Advanced
        /// <summary>
        /// type of organization the item will have
        /// </summary>
        public OrganizeType organize;

        /// <summary>
        /// if the content is meant to be centralized in the width
        /// </summary>
        public bool centralizedWidth;

        /// <summary>
        /// if the content is meant to be centralized in the width
        /// </summary>
        public bool centralizedHeight;
        #endregion

        #region Content
        /// <summary>
        /// type of the content being displayed
        /// </summary>
        public typeContent type;

        private void instanceContent()
        {
            switch (type)
            {
                case typeContent.Image:
                    _contentImage = new ImageContent(this);
                    break;
                case typeContent.Text:
                    _contentText = new TextContent(this);
                    break;
            }
        }

        [SerializeField]
        private TextContent _contentText;
        [SerializeField]
        private ImageContent _contentImage;

        public Content content
        {
            get
            {
                switch (type)
                {
                    case typeContent.Image:
                        _contentImage.Parent = this;
                        return _contentImage;
                    case typeContent.Text:
                        _contentText.Parent = this;
                        return _contentText;
                    default:
                        return null;
                }
            }
        }
        #endregion

        #region Normal
        /// <summary>
        /// Name for indentification. Used to name GameObject and display on inspector
        /// </summary>
        public string name;

        /// <summary>
        /// Color to be used on the display
        /// </summary>
        public Color color;

        /// <summary>
        /// size. Will not be used if Content Size Fitter is true
        /// </summary>
        public float width, height;

        #endregion

        #region Local Variables
        /// <summary>
        /// Local variable that used to know which Content should be updated
        /// </summary>
        public bool update;

        /// <summary>
        /// Local variable to keep track of the rectTransform related to this Content
        /// </summary>
        public RectTransform rectTransform;
        public GameObject gameObject;
        #endregion
    }

    /// <summary>
    /// Class that holds the informatin to display the content as text
    /// </summary>
    [Serializable]
    public class TextContent : Content
    {

        public TextContent(ContentHolder parent)
        {
            Parent = parent;
            font = null;
            fontSize = 32;
            fontStyle = FontStyle.Normal;
            horizontalOverflow = HorizontalWrapMode.Wrap;
            verticalOverflow = VerticalWrapMode.Truncate;
            content = "text";
            bestFit = false;
            contentSizeFitterHorz = false;
            contentSizeFitterVert = false;
            alignment = TextAnchor.UpperLeft;
        }

        [NonSerialized]
        private ContentHolder parent;

        public ContentHolder Parent { get { return parent; } set { parent = value; } }

        public typeContent getType()
        {
            return typeContent.Text;
        }

        /// <summary>
        /// The Font used by the text.
        /// </summary>
        public Font font;

        /// <summary>
        /// The size that the Font should render at.
        /// </summary>
        public int fontSize;

        /// <summary>
        ///  FontStyle used by the text.
        /// </summary>
        public FontStyle fontStyle;

        /// <summary>
        ///  Horizontal overflow mode.
        /// </summary>
        public HorizontalWrapMode horizontalOverflow;

        /// <summary>
        /// Vertical overflow mode.
        /// </summary>
        public VerticalWrapMode verticalOverflow;

        /// <summary>
        /// Content to be displayed 
        /// </summary>
        public string content;

        /// <summary>
        /// If the Text should use the best fit
        /// don't work if the width and height isn't set
        /// </summary>
        public bool bestFit;

        /// <summary>
        /// If the contentSizeFitterComponent and do Vert Should be added
        /// </summary>
        public bool contentSizeFitterVert;

        /// <summary>
        /// If the contentSizeFitterComponent Should be added
        /// </summary>
        public bool contentSizeFitterHorz;

        /// <summary>
        /// Delegated function which will be called (if not null) after the Text is instanciated;
        /// </summary>
        public Action<TextContent, Text> afterInstanced;

        /// <summary>
        /// The positioning of the text reliative to its RectTransform.
        /// </summary>
        public TextAnchor alignment;
    }

    /// <summary>
    /// Class that holds the informatin to display the content as image
    /// </summary>
    [Serializable]
    public class ImageContent : Content
    {
        public ImageContent(ContentHolder parent)
        {
            Parent = parent;
            imageType = Image.Type.Simple;
            preserveAspect = false;
            afterInstanced = null;
        }
        [NonSerialized]
        private ContentHolder parent;
        public ContentHolder Parent { get { return parent; } set { parent = value; } }

        public typeContent getType()
        {
            return typeContent.Image;
        }

        /// <summary>
        /// Sprite that will be used to render the Image
        /// </summary>
        public Sprite spr;

        /// <summary>
        /// Does the preserve aspect on the Image should be on?
        /// </summary>
        public bool preserveAspect;

        /// <summary>
        /// Type of the Image.
        /// </summary>
        public Image.Type imageType;

        /// <summary>
        /// Delegated function which will be called (if not null) after the Image is instanciated;
        /// </summary>
        public Action<ImageContent, Image> afterInstanced;
    }
    #endregion

    #region Setup
    [AutoFind(typeof(Transform), true)]
    [Header("Setup")]
    public RectTransform parent;
    public Font defaultFont;
    public bool rescaleParentWidth, rescaleParentHeight;
    #endregion

    #region Display
    [Header("Display")]
    public Vector2 InitPosition;
    public Vector2 offset;
    public bool centralize = true;
    public TypeOfDisplay typeOfDisplay;

    public Vector2 anchorReference = new Vector2(0, 1);
    public Vector2 pivot = new Vector2(0, 1);
    #endregion

    #region Mode Of Content
    public enum ModeOfContent
    {
        normal,
        advanced,
        stylus,
    }
    public ModeOfContent modeOfContent;
    [HideInInspector]
    public bool advanced;

    #endregion

    [SerializeField, HideInInspector()]
    public List<ContentHolder> listDisplay;
    [SerializeField, HideInInspector(), AutoFind(typeof(ContentListStylus), true)]
    public ContentListStylus stylus;

    ContentListDisplayer()
    {
        if (listDisplay == null)
            listDisplay = new List<ContentHolder>();
    }

    #region Unity Callbacks
    void Start()
    {

        StartCoroutine("_updateAll");
    }

    void OnValidate()
    {
        if (modeOfContent == ModeOfContent.advanced)
            advanced = true;
        else if (modeOfContent == ModeOfContent.stylus)
        {
            if (stylus == null)
            {
                stylus = GetComponent<ContentListStylus>();
            }

            if (stylus != null)
            {
                advanced = GetComponent<ContentListStylus>().advanced;
            }
            else
                advanced = false;
        }
        else
            advanced = false;
    }
    #endregion

    // Use this for initialization

    #region Update
    public void UpdateAll()
    {
        StartCoroutine("_updateAll");
    }

    int redo = 0;
    IEnumerator _updateAll()
    {

        float WT = parent.rect.width, HT = parent.rect.height;
        float maxWidth = -1, maxHeight = -1;
        Vector2 pos = InitPosition;
        for (int i = 0; i < listDisplay.Count; i++)
        {
            if (advanced)
            {
                if (listDisplay[i].centralizedWidth)
                {
                    pos.x = WT / 2 - listDisplay[i].width / 2;
                }
                else if (listDisplay[i].centralizedHeight)
                {
                    pos.y = HT / 2 - listDisplay[i].height / 2;
                }
            }
            else
            {
                if (centralize)
                {
                    if (typeOfDisplay == TypeOfDisplay.InvertColumn || typeOfDisplay == TypeOfDisplay.OneColumn)
                    {
                        pos.x = WT / 2 - listDisplay[i].width / 2;
                    }
                    else if (typeOfDisplay == TypeOfDisplay.InvertRow || typeOfDisplay == TypeOfDisplay.OneRow)
                    {
                        pos.y = HT / 2 - listDisplay[i].height / 2;
                    }
                }
            }


            if (listDisplay[i].rectTransform == null)
            {
                if (modeOfContent == ModeOfContent.stylus)
                {
                    if (stylus == null)
                    {
                        stylus = GetComponent<ContentListStylus>();
                    }
                    stylus.stylizeWithTag(listDisplay[i].tagStylus, listDisplay[i]);
                    redo = 1;
                }
                createContent(pos.x, pos.y, listDisplay[i]);
                redo = 1;
            }
            else if (listDisplay[i].update)
            {
                if (modeOfContent == ModeOfContent.stylus)
                {
                    if (stylus == null)
                    {
                        stylus = GetComponent<ContentListStylus>();
                    }
                    stylus.stylizeWithTag(listDisplay[i].tagStylus, listDisplay[i]);
                }

                if (listDisplay[i].type == typeContent.Image)
                {
                    updateImage((ImageContent)listDisplay[i].content);
                }
                else if (listDisplay[i].type == typeContent.Text)
                {
                    updateText((TextContent)listDisplay[i].content);
                    if (((TextContent)listDisplay[i].content).contentSizeFitterVert || ((TextContent)listDisplay[i].content).contentSizeFitterHorz)
                        redo = 2;
                }
                listDisplay[i].update = false;

                listDisplay[i].rectTransform.localPosition = new Vector3(pos.x, -pos.y, 0);
                listDisplay[i].width = listDisplay[i].rectTransform.rect.width;
                listDisplay[i].height = listDisplay[i].rectTransform.rect.height;
            }
            else
            {
                listDisplay[i].rectTransform.localPosition = new Vector3(pos.x, -pos.y, 0);
                listDisplay[i].width = listDisplay[i].rectTransform.rect.width;
                listDisplay[i].height = listDisplay[i].rectTransform.rect.height;
            }


            if (advanced)
            {
                switch (listDisplay[i].organize)
                {
                    case OrganizeType.continueColumn:
                        pos.x += listDisplay[i].width + offset.x;
                        break;
                    case OrganizeType.continueRow:
                        pos.y += listDisplay[i].height + offset.y;
                        break;
                    case OrganizeType.continueRowColumn:
                        pos.y += listDisplay[i].height + offset.y;
                        pos.x += listDisplay[i].width + offset.x;
                        break;
                    case OrganizeType.jumpColumn:
                        pos.x += listDisplay[i].width + offset.x;
                        pos.y = InitPosition.y;
                        break;
                    case OrganizeType.jumpRow:
                        pos.x = InitPosition.x;
                        pos.y += listDisplay[i].height + offset.y;
                        break;
                    case OrganizeType.nothing:
                        break;
                }
            }
            else
            {
                if (typeOfDisplay == TypeOfDisplay.OneColumn)
                {
                    pos.y += listDisplay[i].height + offset.y;
                }
                else if (typeOfDisplay == TypeOfDisplay.InvertColumn)
                {
                    pos.y -= listDisplay[i].height + offset.y;
                }
                else if (typeOfDisplay == TypeOfDisplay.OneRow)
                {
                    pos.x += listDisplay[i].width + offset.x;
                }
                else if (typeOfDisplay == TypeOfDisplay.InvertRow)
                {
                    pos.x -= listDisplay[i].width + offset.x;
                }
            }

            if (pos.x > maxWidth)
            {
                maxWidth = pos.x;
            }
            if (pos.y > maxHeight)
            {
                maxHeight = pos.y;
            }
        }

        if (rescaleParentWidth)
        {
            parent.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, maxWidth);
        }
        if (rescaleParentHeight)
        {
            parent.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, maxHeight);
        }

        if (redo > 0)
        {
            redo--;
            yield return new WaitForEndOfFrame();
            StartCoroutine("_updateAll");
        }
        yield return null;
    }
    #endregion

    GameObject createContent(float x, float y, ContentHolder content)
    {
        GameObject go = new GameObject(content.name);
        go.transform.SetParent(parent, false);
        RectTransform rect = go.AddComponent<RectTransform>();
        rect.pivot = pivot;
        rect.anchorMax = anchorReference;
        rect.anchorMin = anchorReference;
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, content.width);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, content.height);
        rect.localPosition = new Vector3(-x, -y, 0);
        content.rectTransform = rect;
        content.gameObject = go;
        if (content.type != typeContent.Blank)
            content.content.Parent = content;
        return go;
    }

    #region Update Context
    void updateText(TextContent content)
    {
        GameObject go = content.Parent.gameObject;

        Text text = go.GetComponent<Text>();
        if (text == null)
        {
            text = go.AddComponent<Text>();
        }

        if (content.fontSize == 0)
        {
            Debug.LogError("Can't render text wit size 0, going with 12");
            content.fontSize = 12;
        }
        text.fontSize = content.fontSize;
        if (content.font == null)
        {
            content.font = defaultFont;
        }
        text.font = content.font;
        text.fontStyle = content.fontStyle;
        text.text = content.content;
        text.verticalOverflow = content.verticalOverflow;
        text.horizontalOverflow = content.horizontalOverflow;
        text.resizeTextForBestFit = content.bestFit;
        text.alignment = content.alignment;
        text.color = content.Parent.color;
        if (content.contentSizeFitterVert || content.contentSizeFitterHorz)
        {
            ContentSizeFitter c = go.AddComponent<ContentSizeFitter>();
            if (content.contentSizeFitterHorz)
                c.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            if (content.contentSizeFitterVert)
                c.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

        }
    }

    void updateImage(ImageContent content)
    {

        GameObject go = content.Parent.gameObject;

        Image img = go.GetComponent<Image>();
        if (img == null)
        {
            img = go.AddComponent<Image>();
        }

        img.sprite = content.spr;
        img.preserveAspect = content.preserveAspect;
        img.type = content.imageType;
        img.color = content.Parent.color;

        if (content.afterInstanced != null)
            content.afterInstanced(content, img);


    }

    #endregion

    #region I/O

    [ContextMenu("WRITEXMLTEST")]
    void writeXMLTest()
    {
        writeXML("xmlContentTest");
    }

    [ContextMenu("READXMLTEST")]
    void readXMLTest()
    {
        readXMLFromPath("xmlContentTest");
    }

    void writeXML(string path)
    {
        using (XmlWriter writer = XmlWriter.Create(path))
        {
            writer.WriteStartDocument();
            writer.WriteStartElement("ContentListDisplayer");

            writer.WriteAttributeString("ModeOfContent", modeOfContent.ToString());
            writer.WriteElementString("rescaleParentWidth", rescaleParentWidth.ToString());
            writer.WriteElementString("rescaleParentHeight", rescaleParentHeight.ToString());

            writer.WriteElementString("InitPosition", InitPosition.ToString());
            writer.WriteElementString("offset", offset.ToString());
            writer.WriteElementString("centralize", centralize.ToString());
            writer.WriteElementString("typeOfDisplay", typeOfDisplay.ToString());

            writer.WriteElementString("anchorReference", anchorReference.ToString());
            writer.WriteElementString("pivot", pivot.ToString());

            for (int i = 0; i < listDisplay.Count; i++)
            {
                writer.WriteStartElement("content");
                writer.WriteAttributeString("type", listDisplay[i].type.ToString());
                writer.WriteAttributeString("name", listDisplay[i].name);


                if (modeOfContent == ModeOfContent.stylus)
                {
                    writer.WriteElementString("tagStylus", listDisplay[i].tagStylus);
                }
                else
                {
                    writer.WriteElementString("width", listDisplay[i].width.ToString());
                    writer.WriteElementString("height", listDisplay[i].height.ToString());
                    writer.WriteElementString("color", listDisplay[i].color.ToString());

                    if (modeOfContent == ModeOfContent.advanced)
                    {
                        writer.WriteElementString("centralizedWidth", listDisplay[i].centralizedWidth.ToString());
                        writer.WriteElementString("centralizedHeight", listDisplay[i].centralizedHeight.ToString());
                        writer.WriteElementString("organize", listDisplay[i].organize.ToString());
                    }

                }


                if (listDisplay[i].type == typeContent.Image)
                {
                    ImageContent img = (ImageContent)listDisplay[i].content;
                    writer.WriteElementString("spr", UnityEditor.AssetDatabase.GetAssetPath(img.spr));
                    if (modeOfContent != ModeOfContent.stylus)
                    {
                        writer.WriteElementString("imageType", img.imageType.ToString());
                        writer.WriteElementString("preserveAspect", img.preserveAspect.ToString());
                    }

                }
                else if (listDisplay[i].type == ContentListDisplayer.typeContent.Text)
                {
                    TextContent txt = (TextContent)listDisplay[i].content;

                    writer.WriteElementString("content", txt.content);
                    if (modeOfContent != ModeOfContent.stylus)
                    {
                        writer.WriteElementString("bestFit", txt.bestFit.ToString());
                        writer.WriteElementString("contentSizeFitterHorz", txt.contentSizeFitterHorz.ToString());
                        writer.WriteElementString("contentSizeFitterVert", txt.contentSizeFitterVert.ToString());
                        writer.WriteElementString("font", UnityEditor.AssetDatabase.GetAssetPath(txt.font));
                        writer.WriteElementString("fontSize", txt.fontSize.ToString());
                        writer.WriteElementString("fontStyle", txt.fontStyle.ToString());
                        writer.WriteElementString("horizontalOverflow", txt.horizontalOverflow.ToString());
                        writer.WriteElementString("verticalOverflow", txt.verticalOverflow.ToString());
                        writer.WriteElementString("alignment", txt.alignment.ToString());
                    }

                }



                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }
    }

    void readXMLFromPath(string path)
    {
        using (System.IO.StreamReader reader = new System.IO.StreamReader(path))
        {
            readXMLFromString(reader.ReadToEnd());
        }
    }
    void readXMLFromString(string xml)
    {
        XmlDocument xmlDoc = new XmlDocument();
        //try
        //{
            xmlDoc.LoadXml(xml);
            XmlElement doc = xmlDoc["ContentListDisplayer"];
            modeOfContent = doc.GetAttribute("ModeOfContent").ToEnum<ModeOfContent>();

            rescaleParentWidth = bool.Parse(doc["rescaleParentWidth"].InnerXml);
            rescaleParentHeight = bool.Parse(doc["rescaleParentHeight"].InnerXml);

            InitPosition = doc["InitPosition"].InnerXml.toVector2();
            offset = doc["offset"].InnerXml.toVector2();
            centralize = bool.Parse(doc["centralize"].InnerXml);
            typeOfDisplay = doc["typeOfDisplay"].InnerXml.ToEnum<TypeOfDisplay>();

            anchorReference = doc["anchorReference"].InnerXml.toVector2();
            pivot = doc["pivot"].InnerXml.toVector2();


            XmlNodeList listContents = doc.SelectNodes("content");
            listDisplay = new List<ContentHolder>();
            for (int i = 0; i < listContents.Count; i++)
            {
                ContentHolder content;
                content = new ContentHolder(listContents[i].Attributes.GetNamedItem("type").InnerXml.ToEnum<typeContent>());
                content.name = listContents[i].Attributes.GetNamedItem("name").InnerXml;

                if (modeOfContent == ModeOfContent.stylus)
                {
                    content.tagStylus = listContents[i]["tagStylus"].InnerXml;
                }
                else
                {
                    content.width = float.Parse(listContents[i]["width"].InnerXml);
                    content.height = float.Parse(listContents[i]["height"].InnerXml);
                    content.color = listContents[i]["color"].InnerXml.toColor();


                    if (modeOfContent == ModeOfContent.advanced)
                    {
                        content.centralizedWidth = bool.Parse(listContents[i]["centralizedWidth"].InnerXml);
                        content.centralizedHeight = bool.Parse(listContents[i]["centralizedHeight"].InnerXml);
                        content.organize = listContents[i]["organize"].InnerXml.ToEnum<OrganizeType>();
                    }

                }


                if (content.type == typeContent.Image)
                {
                    ImageContent img = (ImageContent)content.content;

                    img.spr = UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>(listContents[i]["spr"].InnerXml);

                    if (modeOfContent != ModeOfContent.stylus)
                    {
                        img.imageType = listContents[i]["imageType"].InnerXml.ToEnum<Image.Type>();
                        img.preserveAspect = bool.Parse(listContents[i]["preserveAspect"].InnerXml);
                    }

                }
                else if (content.type == ContentListDisplayer.typeContent.Text)
                {
                    TextContent txt = (TextContent)content.content;


                    txt.content = listContents[i]["content"].InnerXml;
                    if (modeOfContent != ModeOfContent.stylus)
                    {

                        txt.bestFit = bool.Parse(listContents[i]["bestFit"].InnerXml);
                        txt.contentSizeFitterHorz = bool.Parse(listContents[i]["contentSizeFitterHorz"].InnerXml);
                        txt.contentSizeFitterVert = bool.Parse(listContents[i]["contentSizeFitterVert"].InnerXml);

                        txt.font = UnityEditor.AssetDatabase.LoadAssetAtPath<Font>(listContents[i]["font"].InnerXml);


                        txt.fontSize = int.Parse(listContents[i]["fontSize"].InnerXml);

                        txt.fontStyle = listContents[i]["fontStyle"].InnerXml.ToEnum<FontStyle>();

                        txt.horizontalOverflow = listContents[i]["horizontalOverflow"].InnerXml.ToEnum<HorizontalWrapMode>();
                        txt.verticalOverflow = listContents[i]["verticalOverflow"].InnerXml.ToEnum<VerticalWrapMode>();
                        txt.alignment = listContents[i]["alignment"].InnerXml.ToEnum<TextAnchor>();

                    }

                }
                listDisplay.Add(content);
            }

        //}
        //catch (Exception e)
        //{
        //    if (e != null)
        //        Debug.LogError("Deu algum bretch = " + e.Data);
        //    else
        //        Debug.LogError("Deu algum bretch");

        //}

    }
    #endregion



}
*/