﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SoundComponent)), CanEditMultipleObjects]
public class SoundComponentEditor : Editor
{

    SoundComponent sound;

    public void Awake()
    {
        sound = (SoundComponent)target;
    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("useAudioFromAudioManager"));
        if (sound.useAudioFromAudioManager)
        {
            if (Application.isPlaying)
            {
                sound.AudioName = EditorGUILayout.TextField("Audio Name", sound.AudioName);
            }
            else
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("audioName"));
            }

        }
        else
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("pitch"));
        }
        EditorGUILayout.PropertyField(serializedObject.FindProperty("PlayOnAwake"));

        serializedObject.ApplyModifiedProperties();
    }
}
