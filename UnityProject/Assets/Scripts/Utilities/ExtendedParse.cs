﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ExtendedParse
{

    #region Color 

    static public Color toColor(this string rString)
    {
        ///RGBA(0.5f,0.5f,0.5f,0.5f)
        string[] temp = rString.Substring(5, rString.Length - 6).Split(',');
        Color renderer = Color.white;
        for (int i = 0; i < temp.Length; i++)
            renderer[i] = float.Parse(temp[i]);

        return renderer;
    }

    #endregion

    #region Vector
    static public Vector4 toVector4(this string rString)
    {
        string[] temp = rString.Substring(1, rString.Length - 2).Split(',');
        Vector4 renderer = new Vector4(float.Parse(temp[0]), float.Parse(temp[1]), float.Parse(temp[2]), float.Parse(temp[4]));

        return renderer;
    }

    static public Vector3 toVector3(this string rString)
    {
        string[] temp = rString.Substring(1, rString.Length - 2).Split(',');
        Vector3 renderer = new Vector3(float.Parse(temp[0]), float.Parse(temp[1]), float.Parse(temp[2]));

        return renderer;
    }
    static public Vector2 toVector2(this string rString)
    {
        string[] temp = rString.Substring(1, rString.Length - 2).Split(',');
        Vector2 renderer = new Vector3(float.Parse(temp[0]), float.Parse(temp[1]));

        return renderer;
    }

    #endregion

    #region Enum
    public static T ToEnum<T>(this string value, bool ignoreCase = true)
    {
        return (T)System.Enum.Parse(typeof(T), value,ignoreCase);
    }

    #endregion
}
