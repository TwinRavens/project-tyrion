﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallText : MonoBehaviour
{
    private void Update()
    {
        transform.position = new Vector3(transform.parent.position.x, transform.parent.position.y + 0.5f, transform.parent.position.z);
        transform.LookAt(Camera.main.transform);
        transform.eulerAngles = new Vector3(45f, transform.eulerAngles.y + 180f, 0f);
    }
}
