#include <SoftwareSerial.h>
#include <SerialCommand.h>

SerialCommand sCmd;

void pingHandler () {
  Serial.println("PONG");
}

void setup() {
  Serial.begin(9600);
  while (!Serial);

  sCmd.addCommand("PING", pingHandler);
}

void loop () {
  if (Serial.available() > 0)
    sCmd.readSerial();
}

/*void echoHandler () {
  char *arg;
  arg = sCmd.next();
  if (arg != NULL)
    Serial.println(arg);
  else
    Serial.println("nothing to echo");
}*/
