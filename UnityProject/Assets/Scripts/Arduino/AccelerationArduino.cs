﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using God.Events;

public class MotionEvent : GameEvent
{
    public MotionEvent(Vector3 acc)
    {
        this.acceleration = acc;
    }

    private Vector3 m_acceleration;
    public Vector3 acceleration
    {
        get { return m_acceleration; }
        private set { m_acceleration = value; }
    }
}

public class StandbyCalibrationDone : GameEvent { }
public class ForwardCalibrationDone : GameEvent { }

public class AccelerationArduino : MonoBehaviour
{

    //===============
    //Motion Checking
    //===============
    List<Vector3> motionAcc;
    public int maxMotionBufferFrames = 4;
    int motionBufferFrames = 0;
    bool inMotion = false;
    int inMotionFrame = 0;
    List<Vector3> inMotionAcc;
    public int inMotionMaxCount = 2;
    public float motionDetectionFactor = 4.0f;
    public float releaseForceRelation = 0.8f;

    //===================
    //Standby calibration
    //===================
    [SerializeField, InspectorReadOnly]
    Vector3 standbyAcc = new Vector3(); // used
    int standbyCalCount = 0;
    public int maxStandbyCalVectors = 50;
    [SerializeField, InspectorReadOnly]
    float standbyStdVarDist = 0; // used

    //=========================
    //Local forward calibration
    //=========================
    [SerializeField, InspectorReadOnly]
    Vector3 localForward = new Vector3(); //used
    Quaternion worldToLocal = Quaternion.identity;
    int forwardCalCount = 0;
    public int maxForwardCalVectors = 4;

    //================
    //Arduino raw data
    //================
    Vector3 rawAcc = new Vector3();
    float delta = 0;
    ulong count;

    //=========================
    //Attack control parameters
    //=========================
    bool outsideRegion = false;
    int ignoredAttacks = 0;
    public int ignoreAttacksBuffer = 3;
    public int detectionMinRange = 3000;

    //=========
    //==Flags==
    //=========
    bool running = false;
    [SerializeField, InspectorReadOnly]
    bool standbyCalibration = false; // used
    [SerializeField, InspectorReadOnly]
    bool forwardCalibration = false; // used
    public bool emulateArduino = false;
    [SerializeField, InspectorReadOnly]
    bool motionChecking = false;
    EventManager eventManager;
    public bool debug = false;
    public string stringDebug = "Needs calibration";

    public int frameCount;

    /// <summary>
    /// Awake for instancing dynamic allocated memory
    /// </summary>
    void Awake()
    {
        frameCount = 0;
        motionAcc = new List<Vector3>();
        inMotionAcc = new List<Vector3>();
    }

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this);
        eventManager = EventManager.Instance;
        eventManager.AddListener<ArduinoReadEvent>(ReadArduino);

        if (debug)
            eventManager.AddListener<GameManager.ThrowBallEvent>((GameManager.ThrowBallEvent e) => { Debug.Log("Attack done! At: " + e.force); });
        if (PlayerPrefs.HasKey("motionChecking"))
        {
            motionChecking = PlayerPrefs.GetInt("motionChecking") == 1;

            localForward = new Vector3(PlayerPrefs.GetFloat("localForwardX"),
            PlayerPrefs.GetFloat("localForwardY"),
            PlayerPrefs.GetFloat("localForwardZ"));

            standbyStdVarDist = PlayerPrefs.GetFloat("standbyStdVarDist");

            standbyAcc = new Vector3(PlayerPrefs.GetFloat("standbyAccX"),
            PlayerPrefs.GetFloat("standbyAccY"),
            PlayerPrefs.GetFloat("standbyAccZ"));
        }
        if (motionChecking)
        {
            ArduinoInterface.Instance.ConnectToArduino();
            stringDebug = "Calibrated";
            running = true;
        }
        else
            stringDebug = "Needs calibration";

    }

    public void Save()
    {
        PlayerPrefs.SetInt("motionChecking", motionChecking ? 1 : 0);

        PlayerPrefs.SetFloat("localForwardX", localForward.x);
        PlayerPrefs.SetFloat("localForwardY", localForward.y);
        PlayerPrefs.SetFloat("localForwardZ", localForward.z);

        PlayerPrefs.SetFloat("standbyStdVarDist", standbyStdVarDist);

        PlayerPrefs.SetFloat("standbyAccX", standbyAcc.x);
        PlayerPrefs.SetFloat("standbyAccY", standbyAcc.y);
        PlayerPrefs.SetFloat("standbyAccZ", standbyAcc.z);

    }

    void ReadArduino(ArduinoReadEvent data)
    {
        if (!running)
            return;

        if (data.read.Length < 3)
            return;

        if (data.read[0] != 'a' || data.read[1] != 'c' || data.read[2] != 'c')
            return;


        string[] s = data.read.Split('\t');

        try
        {
            if (s.Length < 5)
                return;

            rawAcc.x = float.Parse(s[1]);
            rawAcc.y = float.Parse(s[2]);
            rawAcc.z = float.Parse(s[3]);
            count = ulong.Parse(s[4]);
            delta = float.Parse(s[5]);

            rawAcc /= count;


        }
        catch (Exception e)
        {
            Debug.LogError("Couldn't parse " + data.read + " " + e);
        }

        if (standbyCalibration)
        {
            standbyAcc += rawAcc;
            standbyCalCount++;

            Debug.Log("Standby Calibration in " + (standbyCalCount * 100.0f) / maxStandbyCalVectors + "% done...");
            stringDebug = "Standby Calibration in " + ((standbyCalCount * 100.0f) / maxStandbyCalVectors).ToString("0.0") + "%";

            motionAcc.Add(rawAcc);

            if (standbyCalCount == maxStandbyCalVectors)
            {
                standbyAcc /= standbyCalCount;
                standbyCalibration = false;
                standbyCalCount = 0;

                standbyStdVarDist = 0;
                for (int i = 0; i < motionAcc.Count; i++)
                {
                    standbyStdVarDist += (motionAcc[i] - standbyAcc).sqrMagnitude;
                }
                standbyStdVarDist /= (motionAcc.Count - 1);

                standbyStdVarDist = Mathf.Sqrt(standbyStdVarDist);

                Debug.Log("Standby calibration done! Standby Acceleration is: " + standbyAcc);
                stringDebug = "Now do " + maxForwardCalVectors + " attacks forward";
                eventManager.TriggerEvent(new StandbyCalibrationDone());
            }

            return;
        }

        Vector3 baseAcc = rawAcc - standbyAcc;

        if (forwardCalibration)
        {
            Vector3 xzAcc = new Vector3(baseAcc.x, 0, baseAcc.y);

            if (xzAcc.magnitude > detectionMinRange)
            {
                if (!outsideRegion)
                {

                    localForward += xzAcc;
                    forwardCalCount++;

                    Debug.Log("Forward cal " + forwardCalCount + ": " + xzAcc);
                    stringDebug = "Now do " + (maxForwardCalVectors - forwardCalCount) + " attacks forward";
                    //Finished calibrating
                    if (forwardCalCount == maxForwardCalVectors)
                    {
                        localForward /= forwardCalCount;
                        forwardCalCount = 0;
                        forwardCalibration = false;
                        motionChecking = true;
                        worldToLocal = Quaternion.FromToRotation(localForward, Vector3.forward);
                        stringDebug = "Calibration done. Press 'play' to Play.";
                        Debug.Log("Forward calibration done! Local forward as: " + localForward);
                        eventManager.TriggerEvent(new ForwardCalibrationDone());
                        Save();
                    }

                    outsideRegion = true;

                    return;
                }
                //Debug.Log("Peak with magnitude " + xzAcc.magnitude + ", " + " values = " + xzAcc);
            }
            else
            {
                if (outsideRegion)
                {
                    //Be sure to ignore the N following frames
                    ignoredAttacks++;
                    if (ignoredAttacks == ignoreAttacksBuffer)
                    {
                        outsideRegion = false;
                        ignoredAttacks = 0;
                    }
                }
            }
        }


        if (motionChecking)
        {
            frameCount++;

            if ((baseAcc.magnitude / standbyStdVarDist) > motionDetectionFactor)
                motionBufferFrames++;
            else
                motionBufferFrames = 0;

            inMotion = motionBufferFrames >= maxMotionBufferFrames;

            if (!inMotion)
            {
                if (inMotionFrame == frameCount - 1)
                {
                    Vector3[] mostExpressive = new Vector3[1];

                    for (int i = 0; i < inMotionAcc.Count; i++)
                    {
                        Vector3 localDir = worldToLocal * inMotionAcc[i];
                        for (int j = 0; j < mostExpressive.Length; j++)
                        {
                            if (localDir.z > mostExpressive[j].z)
                            {
                                mostExpressive[j] = localDir;
                                break;
                            }
                        }
                    }

                    Vector3 medianDir = Vector3.zero;
                    for (int i = 0; i < mostExpressive.Length; i++)
                    {
                        medianDir += mostExpressive[i];
                    }
                    medianDir /= mostExpressive.Length;

                    if (Vector3.Dot(medianDir, Vector3.forward) < 0)
                    {
                        Debug.LogError("Median dir not pointing Forward!");
                        return;
                    }

                    Debug.Log("Median value is " + medianDir);
                    if (medianDir.magnitude / baseAcc.magnitude > releaseForceRelation)
                    {
                        Debug.Log("Released!");

                        medianDir *= Mathf.Abs(Physics.gravity.y) / 8192.0f;

                        eventManager.TriggerEvent(new GameManager.ThrowBallEvent(medianDir));
                    }

                    inMotionAcc.Clear();

                    return;
                }

                if (debug)
                    Debug.Log("Standing Still...");
            }
            else
            {
                inMotionFrame = frameCount;

                if (Vector3.Dot(baseAcc, localForward) > 0)
                {
                    inMotionAcc.Add(baseAcc);
                }

                //Keep sum in size count
                if (inMotionAcc.Count > inMotionMaxCount)
                    inMotionAcc.RemoveAt(0);

                if (debug)
                    Debug.Log("Moving...");
            }
        }
    }

    public void StartCalibration()
    {
        running = true;
        standbyCalibration = true;
        forwardCalibration = true;
        localForward = new Vector3();
        worldToLocal = Quaternion.identity;
        standbyAcc = new Vector3();
    }

    private void Update()
    {
    

        if (emulateArduino)
        {
            // if (Input.anyKey)
            // WorldToLocal(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));
        }
    }
}
