﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorfulScript : MonoBehaviour
{

    [AutoFind(typeof(Graphic), true)]
    public Graphic t;
    private Color c = Color.white;
    //public float where;
    //public int state;
    //public float seconds;
    public float secondsToAll;
    private float currentHue;
    private float currentSat;
    private float currentValue;

    public Color baseColor;
    void Start()
    {
        c = baseColor;
        Color.RGBToHSV(baseColor, out currentHue, out currentSat, out currentValue);

        //c = new Color(1, 75/ 255.0f, 75 / 255.0f);
        //state = 0;
        //where = 75 / 255.0f;

    }

    // Update is called once per frame
    void Update()
    {
        currentHue += 1 / secondsToAll * Time.deltaTime;
        currentHue %= 1;
        t.color = Color.HSVToRGB(currentHue, currentSat, currentValue);
        /*switch (state)
        {
            case 0:
                c.b += 1 * Time.deltaTime * 1/seconds;
                if (c.b >= 1)
                {
                    c.b = 1;
                    state = 1;
                }
                break;

            case 1:
                c.r -= 1 * Time.deltaTime * 1 / seconds;
                if (c.r <= where)
                {
                    c.r = where;
                    state = 2;
                }
                break;

            case 2:
                c.g += 1 * Time.deltaTime * 1 / seconds;
                if (c.g >= 1)
                {
                    c.g = 1;
                    state = 3;
                }
                break;

            case 3:
                c.b -= 1 * Time.deltaTime * 1 / seconds;
                if (c.b <= where)
                {
                    c.b = where;
                    state = 4;
                }
                break;

            case 4:
                c.r += 1 * Time.deltaTime * 1 / seconds;
                if (c.r >= 1)
                {
                    c.r = 1;
                    state = 5;
                }
                break;

            case 5:
                c.g -= 1 * Time.deltaTime * 1 / seconds;
                if (c.g <= where)
                {
                    c.g = where;
                    state = 0;
                }
                break;

        }
        t.color = c;
        */
    }
}
