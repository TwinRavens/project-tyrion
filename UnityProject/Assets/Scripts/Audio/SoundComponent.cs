﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundComponent : MonoBehaviour
{

    /// <summary>
    /// If this sound component wants to use one sound stored on the AudioManager, but playing it here.
    /// </summary>
    public bool useAudioFromAudioManager;
    [SerializeField]
    string audioName;
    public string AudioName
    {
        get { return audioName; }
        set
        {
            if (AudioName != value)
            {
            }
        }
    }
    Sound sound;

    [MinMaxRange()]
    public MinMaxRange pitch;
    [HideInInspector]
    public AudioSource source;
    public bool PlayOnAwake;
    AudioManager manager;
    void Awake()
    {
        manager = AudioManager.Instance;
        source = GetComponent<AudioSource>();
        if (PlayOnAwake)
            Play();
        loadAudio();
    }

    public void loadAudio()
    {
        sound = manager.GetAudio(audioName);

    }
    public void Play()
    {
        if (useAudioFromAudioManager && sound != null)
        {
            sound.Play(source);
        }
        source.pitch = pitch.GetRandomValue();
        source.Play();
    }

    public void Play(string tag, bool oneshoot = false)
    {
        manager.GetAudio(tag).Play(source, oneshoot);
    }
}
