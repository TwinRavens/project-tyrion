﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;

public class SerialPortSelector : MonoBehaviour
{

    public TMPro.TextMeshProUGUI text;
    [SerializeField, InspectorReadOnly]
    string[] ports;
    [SerializeField, InspectorReadOnly]
    public int currentPort;


    public void Start()
    {
        currentPort = -1;
        ports = SerialPort.GetPortNames();
        for (int i = 0; i < ports.Length; i++)
        {
            if (ports[i] == ArduinoInterface.Instance.portName)
            {
                currentPort = i;
                break;
            }
        }
        if (currentPort == -1)
        {
            currentPort = 0;
        }
        if (ports.Length == 0)
        {
            text.text = "No serial port available";
        }
        else
        {
            text.text = ports[currentPort];
        }

    }

    public void UpdateNames()
    {
        string[] oldPort = ports;
        ports = SerialPort.GetPortNames();
        bool foundCorrespondent = false;

        for (int i = 0; i < ports.Length; i++)
        {
            if (ports[i] == oldPort[currentPort] || ports[i] == ArduinoInterface.Instance.portName)
            {
                currentPort = i;
                foundCorrespondent = true;
                break;
            }
        }

        if (!foundCorrespondent)
        {
            currentPort = 0;
        }
        if (ports.Length == 0)
        {
            text.text = "No serial port available";
        }
        else
        {
            text.text = ports[currentPort];
        }



    }


    public void Next()
    {
        currentPort++;
        currentPort %= ports.Length;
        text.text = ports[currentPort];
    }

    public void Previous()
    {
        currentPort--;
        if (currentPort < 0)
            currentPort += ports.Length;
        text.text = ports[currentPort];
    }

    public string getCurrentPort()
    {
        return ports[currentPort];
    }
}
