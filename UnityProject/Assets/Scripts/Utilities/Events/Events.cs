﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace God.Events
{
    public class GameEvent    {    }

    #region Examples
    public class ExampleEvent : GameEvent
    {

    }

    public class ExampleEventWithProperties : GameEvent
    {
        public float _myFloat;

        public ExampleEventWithProperties(float f)
        {
            _myFloat = f;	
        }
    }
    #endregion

}