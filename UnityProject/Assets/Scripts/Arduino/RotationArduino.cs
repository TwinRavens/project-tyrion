﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationArduino : MonoBehaviour
{
    Quaternion rotOffset = Quaternion.identity;
    // Use this for initialization
    void Start()
    {
        God.Events.EventManager.Instance.AddListener<ArduinoReadEvent>(ReadArduino);
        rotOffset = transform.rotation;
    }

    float x, y, z, w, delta;
    void ReadArduino(ArduinoReadEvent data)
    {
        if (data.read[0] != 'q' || data.read[1] != 'u' || data.read[2] != 'a' || data.read[3] != 't')
            return;

        string[] s = data.read.Split('\t');
        

        try
        {
            x = float.Parse(s[1]);
            y = float.Parse(s[2]);
            z = float.Parse(s[3]);
            w = float.Parse(s[4]);
            delta = float.Parse(s[5]);
 
            transform.rotation = new Quaternion(x, y, z, w) * Quaternion.Inverse(rotOffset);
        }
        catch 
        {
            Debug.Log("Couldn't parse " + data.read);
        }




    }
}
