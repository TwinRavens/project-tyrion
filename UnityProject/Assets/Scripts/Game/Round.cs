﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[System.Serializable]
public struct Round
{
    public int team;
    public int score;
    public Round(int team, int score)
    {
        this.team = team;
        this.score = score;
    }
    /// <summary>
    /// will return the score.
    /// </summary>
    /// <param name="scoreboard">Rounds Array</param>
    /// <returns>value[0] = Player 1, value[1] = Player 2</returns>
    public int[] Scoreboard(List<Round> scoreboard)
    {
        int[] value = new int[2];

        for (int i = 0; i < scoreboard.Count; i++)
        {
            if (scoreboard[i].team == 1)
                value[0] += scoreboard[i].score;
            else
                value[1] += scoreboard[i].score;
        }

        return value;
    }
}