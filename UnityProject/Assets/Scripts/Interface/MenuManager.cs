﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{

    public GameManager gameManager;
    public GameObject rootCamera;

    public SerialPortSelector serialPortSelector;

    public void StartGame()
    {
        rootCamera.transform.rotation = Quaternion.identity;
        rootCamera.GetComponent<AutoRotate3D>().enabled = false;
        gameManager.StartGame();
        gameObject.SetActive(false);
    }

    public void Credits()
    {

    }

    public void Options()
    {

    }


    public void GoToSite()
    {
        Application.OpenURL("https://www.facebook.com/TwinRavensDev/");
    }

    public void RecconetToArduino()
    {
        ArduinoInterface.Instance.portName = serialPortSelector.getCurrentPort();
        ArduinoInterface.Instance.ConnectToArduino();
    }
}
