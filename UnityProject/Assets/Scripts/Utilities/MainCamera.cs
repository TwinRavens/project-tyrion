﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : Singleton<MainCamera>
{
    public new Camera camera;
    void Start()
    {
        camera = GetComponent<Camera>();
        enabled = false;
    }
}

