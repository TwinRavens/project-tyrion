﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentListStylus : MonoBehaviour
{


    public bool advanced = false;
    [HideInInspector]
    public List<ContentListDisplayer.ContentHolder> listStylus;


    public ContentListDisplayer.ContentHolder getContent(string tag)
    {
        for(int i = 0; i < listStylus.Count; i++)
        {
            if(listStylus[i].name == tag)
            {
                return listStylus[i];
            }
        }
        return null;
    }

    public void stylizeWithTag(string tag, ContentListDisplayer.ContentHolder content)
    {
        ContentListDisplayer.ContentHolder baseContent = getContent(tag);
        if (baseContent == null)
            return;

        content.color = baseContent.color;
        content.width = baseContent.width;
        content.height = baseContent.height;
        content.centralizedHeight = baseContent.centralizedHeight;
        content.centralizedWidth = baseContent.centralizedWidth;
        content.organize = baseContent.organize;

        if (baseContent.type == ContentListDisplayer.typeContent.Image)
        {
            ContentListDisplayer.ImageContent img = (ContentListDisplayer.ImageContent)content.content;
            ContentListDisplayer.ImageContent imgBase = (ContentListDisplayer.ImageContent)baseContent.content;
            img.afterInstanced = imgBase.afterInstanced;
            img.imageType = imgBase.imageType;
            img.preserveAspect = imgBase.preserveAspect;

        }
        else if (baseContent.type == ContentListDisplayer.typeContent.Text)
        {
            ContentListDisplayer.TextContent txt = (ContentListDisplayer.TextContent)content.content;
            ContentListDisplayer.TextContent txtBase = (ContentListDisplayer.TextContent)baseContent.content;
            txt.afterInstanced = txtBase.afterInstanced;
            txt.bestFit = txtBase.bestFit;
            txt.contentSizeFitterHorz = txtBase.contentSizeFitterHorz;
            txt.contentSizeFitterVert = txtBase.contentSizeFitterVert;
            txt.font = txtBase.font;
            txt.fontSize = txtBase.fontSize;
            txt.fontStyle = txtBase.fontStyle;
            txt.horizontalOverflow = txtBase.horizontalOverflow;
            txt.verticalOverflow = txtBase.verticalOverflow;
            txt.alignment = txtBase.alignment;

        }


    }

}
*/