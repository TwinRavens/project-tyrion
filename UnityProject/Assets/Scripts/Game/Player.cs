﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public struct Player
{
	public Color color;
	public int score;
	public int ballCounter;
	public void ResetRound()
	{
		ballCounter = 0;
	}
	/// <summary>
	/// Returns true if you have a winner;
	/// </summary>
	/// <param name="players">Pass a structure that stores the players.</param>
	/// <param name="score">What is the required score for a player to win?</param>
	/// <returns></returns>
	static public bool Winner(Player[] players, int score)
	{
		for (int i = 0; i < players.Length; i++)
			if (players[i].score >= score)
				return true;
		return false;
	}
}
