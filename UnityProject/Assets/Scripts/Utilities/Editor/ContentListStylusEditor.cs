﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(ContentListStylus))]
public class ContentListStylusEditor : Editor
{

    private ReorderableList list;

    List<float> heights;
    ContentListStylus targetO;


    void OnEnable()
    {
        targetO = (ContentListStylus)target;
        if (targetO.listStylus == null)
        {
            targetO.listStylus = new List<ContentListDisplayer.ContentHolder>();
        }
        heights = new List<float>(targetO.listStylus.Count);

        list = new ReorderableList(serializedObject,
                serializedObject.FindProperty("listStylus"),
                true, true, true, true);

        list.drawHeaderCallback = rect =>
        {
            EditorGUI.LabelField(rect, "Content");
        };



        list.drawElementCallback =
    (Rect rect, int index, bool isActive, bool isFocused) =>
    {
        var element = list.serializedProperty.GetArrayElementAtIndex(index);
        ContentListDisplayer.typeContent type = targetO.listStylus[index].type;


        float height = EditorGUIUtility.singleLineHeight * 2f;
        if (isActive)
        {
            if (targetO.advanced)
                height += EditorGUIUtility.singleLineHeight * 3f;

            height += EditorGUIUtility.singleLineHeight * 8;

            if (type == ContentListDisplayer.typeContent.Blank)
                height -= EditorGUIUtility.singleLineHeight * 6;

            if (type == ContentListDisplayer.typeContent.Text)
                height += EditorGUIUtility.singleLineHeight * 2.5f;
        }

        if (heights == null)
        {
            heights = new List<float>();
        }
        if (heights.Count <= index)
        {
            for (int i = heights.Count; i <= index; i++)
                heights.Add(EditorGUIUtility.singleLineHeight * 2f);
        }

        heights[index] = height;

        float margin = height / 10;
        rect.height = height;
        rect.width = rect.width / 2 - margin / 2;

        rect.y += 2;

        rect.height = EditorGUIUtility.singleLineHeight * 10;

        float x, y;
        x = rect.x;
        y = rect.y;

        #region Content Draw

        EditorGUI.LabelField(
               new Rect(x, y, 40, EditorGUIUtility.singleLineHeight),
               "Tag");

        x += 45;
        EditorGUI.PropertyField(
            new Rect(x, y, 200, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("name"), GUIContent.none);

        x += 205;
        EditorGUI.LabelField(
        new Rect(x, y, 200, EditorGUIUtility.singleLineHeight), type.ToString());

        x = rect.x;
        y += EditorGUIUtility.singleLineHeight + 5;
        #endregion

        if (isActive)
        {
            #region Advanced & Normal

            EditorGUI.LabelField(
            new Rect(x, y, 35, EditorGUIUtility.singleLineHeight),
            "Width");

            x += 36;
            EditorGUI.PropertyField(
                new Rect(x, y, 35, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("width"), GUIContent.none);

            x += 40;

            EditorGUI.LabelField(
               new Rect(x, y, 39, EditorGUIUtility.singleLineHeight),
               "Height");

            x += 40;

            EditorGUI.PropertyField(
                new Rect(x, y, 35, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("height"), GUIContent.none);

            x += 40;

            if (type != ContentListDisplayer.typeContent.Blank)
            {

                EditorGUI.LabelField(
       new Rect(x, y, 35, EditorGUIUtility.singleLineHeight),
     "Color");


                x += 36;
                EditorGUI.PropertyField(
                    new Rect(x, y, 60, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("color"), GUIContent.none);

                x += 65;
            }

            #region Advanced
            if (targetO.advanced)
            {
                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 120, EditorGUIUtility.singleLineHeight), "Organize Mode");
                x += 120;

                EditorGUI.PropertyField(
                  new Rect(x, y, 120, EditorGUIUtility.singleLineHeight),
                  element.FindPropertyRelative("organize"), GUIContent.none);

                x += 125;


                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 120, EditorGUIUtility.singleLineHeight), "Centralize   Horz");

                x += 125;

                EditorGUI.PropertyField(
                    new Rect(x, y, 35, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("centralizedWidth"), GUIContent.none);

                x += 30;

                EditorGUI.LabelField(new Rect(x, y, 50, EditorGUIUtility.singleLineHeight), "Vert");

                x += 55;

                EditorGUI.PropertyField(
                    new Rect(x, y, 35, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("centralizedHeight"), GUIContent.none);

            }
            #endregion

            x = 270 + rect.x;
            #endregion

            #region Image Draw
            if (type == ContentListDisplayer.typeContent.Image)
            {
                ContentListDisplayer.ImageContent obj = ((ContentListDisplayer.ImageContent)(targetO.listStylus[index].content));
                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 15;

                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 100, EditorGUIUtility.singleLineHeight), "Image Type");

                x += 105;


                obj.imageType = (UnityEngine.UI.Image.Type)EditorGUI.EnumPopup(new Rect(x, y, 70, EditorGUIUtility.singleLineHeight), obj.imageType);

                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 100, EditorGUIUtility.singleLineHeight), "Preserve Aspect");

                x += 105;

                obj.preserveAspect = EditorGUI.Toggle(new Rect(x, y, 70, EditorGUIUtility.singleLineHeight), obj.preserveAspect);


            }
            #endregion

            #region Text Draw
            else if (type == ContentListDisplayer.typeContent.Text)
            {

                ContentListDisplayer.TextContent obj = ((ContentListDisplayer.TextContent)(targetO.listStylus[index].content));
          

                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 30, EditorGUIUtility.singleLineHeight), "Size");

                x += 35;

                obj.fontSize = EditorGUI.IntField(new Rect(x, y, 75, EditorGUIUtility.singleLineHeight), obj.fontSize);

                x += 100;

                EditorGUI.LabelField(new Rect(x, y, 55, EditorGUIUtility.singleLineHeight), "Best Fit");

                x += 60;

                obj.bestFit = EditorGUI.Toggle(new Rect(x, y, 75, EditorGUIUtility.singleLineHeight), obj.bestFit);

                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 30, EditorGUIUtility.singleLineHeight), "Font");

                x += 35;

                obj.font = EditorGUI.ObjectField(new Rect(x, y, 125, EditorGUIUtility.singleLineHeight), obj.font, typeof(Font), false) as Font;

                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 70, EditorGUIUtility.singleLineHeight), "Font Style");

                x += 70;

                obj.fontStyle = (FontStyle)EditorGUI.EnumPopup(new Rect(x, y, 125, EditorGUIUtility.singleLineHeight), obj.fontStyle);

                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 35, EditorGUIUtility.singleLineHeight), "Overflow");

                x += 37;

                EditorGUI.LabelField(new Rect(x, y, 30, EditorGUIUtility.singleLineHeight), "Horz");

                x += 32;

                obj.horizontalOverflow = (HorizontalWrapMode)EditorGUI.EnumPopup(new Rect(x, y, 60, EditorGUIUtility.singleLineHeight), obj.horizontalOverflow);

                x += 63;

                EditorGUI.LabelField(new Rect(x, y, 30, EditorGUIUtility.singleLineHeight), "Vert");

                x += 32;

                obj.verticalOverflow = (VerticalWrapMode)EditorGUI.EnumPopup(new Rect(x, y, 60, EditorGUIUtility.singleLineHeight), obj.verticalOverflow);


                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 10;

                EditorGUI.LabelField(new Rect(x, y, 150, EditorGUIUtility.singleLineHeight), "Content Size Fitter  Horz");

                x += 150;

                obj.contentSizeFitterHorz = EditorGUI.Toggle(new Rect(x, y, 30, EditorGUIUtility.singleLineHeight), obj.contentSizeFitterHorz);

                x += 30;

                EditorGUI.LabelField(new Rect(x, y, 50, EditorGUIUtility.singleLineHeight), "Vert");

                x += 50;

                obj.contentSizeFitterVert = EditorGUI.Toggle(new Rect(x, y, 30, EditorGUIUtility.singleLineHeight), obj.contentSizeFitterVert);

                x = rect.x;
                y += EditorGUIUtility.singleLineHeight + 5;

                EditorGUI.LabelField(new Rect(x, y, 70, EditorGUIUtility.singleLineHeight), "Alignment");

                x += 75;

                obj.alignment = (TextAnchor)EditorGUI.EnumPopup(new Rect(x, y, 120, EditorGUIUtility.singleLineHeight), obj.alignment);
            }
            #endregion



        }

    };

        list.elementHeightCallback = (index) =>
            {
                Repaint();
                float height = 0;

                try
                {
                    height = heights[index];
                }
                catch (System.ArgumentOutOfRangeException e)
                {
                    height = EditorGUIUtility.singleLineHeight * 2f;
                }


                return height;
            };

        list.onAddDropdownCallback = (Rect buttonRect, ReorderableList l) =>
        {
            var menu = new GenericMenu();

            menu.AddItem(new GUIContent("Image Content"), false, clickHandler, 0);
            menu.AddItem(new GUIContent("Text Content"), false, clickHandler, 1);
            menu.AddItem(new GUIContent("Blank Space"), false, clickHandler, 2);

            List<ContentListDisplayer.ContentHolder> lista = targetO.listStylus;
            for (int i = 0; i < lista.Count; i++)
            {
                menu.AddItem(new GUIContent("Copy From Existing itens/" + i.ToString() + " - " + lista[i].name), false, copyExistingItens, lista[i]);
            }
            menu.ShowAsContext();

        };

        list.onRemoveCallback = (ReorderableList l) =>
        {
            if (EditorUtility.DisplayDialog("Warning!",
                "Are you sure you want to delete the content?", "Yes", "No"))
            {
                ReorderableList.defaultBehaviours.DoRemoveButton(l);
            }
        };


    }
    private void copyExistingItens(object t)
    {

        ContentListDisplayer.ContentHolder baseContent = (ContentListDisplayer.ContentHolder)t;

        ContentListDisplayer.ContentHolder content = new ContentListDisplayer.ContentHolder(baseContent.type);

        content.name = baseContent.name + "_Copy";
        content.color = baseContent.color;
        content.width = baseContent.width;
        content.height = baseContent.height;
        content.centralizedHeight = baseContent.centralizedHeight;
        content.centralizedWidth = baseContent.centralizedWidth;
        content.organize = baseContent.organize;

        if (baseContent.type == ContentListDisplayer.typeContent.Image)
        {
            ContentListDisplayer.ImageContent img = (ContentListDisplayer.ImageContent)content.content;
            ContentListDisplayer.ImageContent imgBase = (ContentListDisplayer.ImageContent)baseContent.content;
            img.afterInstanced = imgBase.afterInstanced;
            img.imageType = imgBase.imageType;
            img.preserveAspect = imgBase.preserveAspect;
            img.spr = imgBase.spr;

            heights.Add(1);
            targetO.listStylus.Add(content);
        }
        else if (baseContent.type == ContentListDisplayer.typeContent.Text)
        {
            ContentListDisplayer.TextContent txt = (ContentListDisplayer.TextContent)content.content;
            ContentListDisplayer.TextContent txtBase = (ContentListDisplayer.TextContent)baseContent.content;
            txt.afterInstanced = txtBase.afterInstanced;
            txt.bestFit = txtBase.bestFit;
            txt.content = txtBase.content;
            txt.contentSizeFitterHorz = txtBase.contentSizeFitterHorz;
            txt.contentSizeFitterVert = txtBase.contentSizeFitterVert;
            txt.font = txtBase.font;
            txt.fontSize = txtBase.fontSize;
            txt.fontStyle = txtBase.fontStyle;
            txt.horizontalOverflow = txtBase.horizontalOverflow;
            txt.verticalOverflow = txtBase.verticalOverflow;
            txt.alignment = txtBase.alignment;


            heights.Add(1);
            targetO.listStylus.Add(content);
        }
        else if (baseContent.type == ContentListDisplayer.typeContent.Blank)
        {
            heights.Add(1);
            targetO.listStylus.Add(content);
        }



        serializedObject.ApplyModifiedProperties();
    }

    private void clickHandler(object t)
    {
        //0 is img 
        //1 is txt
        //2 is blank
        int type = (int)t;

        if (type == 0)
        {
            heights.Add(1);
            ContentListDisplayer.ContentHolder content = new ContentListDisplayer.ContentHolder(ContentListDisplayer.typeContent.Image);
            content.name = "Image Content";

            targetO.listStylus.Add(content);
        }
        else if (type == 1)
        {
            heights.Add(1);
            ContentListDisplayer.ContentHolder content = new ContentListDisplayer.ContentHolder(ContentListDisplayer.typeContent.Text);
            content.name = "Text Content";
            targetO.listStylus.Add(content);
        }
        else if (type == 2)
        {
            heights.Add(1);
            ContentListDisplayer.ContentHolder content = new ContentListDisplayer.ContentHolder(ContentListDisplayer.typeContent.Blank);
            content.name = "Blank";
            targetO.listStylus.Add(content);
        }



        serializedObject.ApplyModifiedProperties();
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();


        DrawDefaultInspector();

        if (list != null)
            list.DoLayoutList();

        EditorGUILayout.Space();

        serializedObject.ApplyModifiedProperties();
    }


}
*/