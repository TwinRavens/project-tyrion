﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(TriggerAudio)), CanEditMultipleObjects]
public class TriggerAudioEditor : Editor
{

    TriggerAudio sound;
    ReorderableList soundList;
    public void Awake()
    {
        sound = (TriggerAudio)target;
        soundList = new ReorderableList(serializedObject, serializedObject.FindProperty("sounds"), true, true, true, true);

        soundList.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Sounds");

            //if (GUI.Button(new Rect(rect.width - 40, rect.y, 20, rect.height), "+"))
            //{
            //    Undo.RecordObject(target, "Added new color palette");
            //    cpc.colorsPalettes.Add(new ColorPalette(cpc.colorsPerPalette));
            //}
            //if (GUI.Button(new Rect(rect.width - 20, rect.y, 20, rect.height), "-"))
            //{
            //    Undo.RecordObject(cpc, "Remove last color palette");
            //    cpc.colorsPalettes.RemoveAt(cpc.colorsPalettes.Count - 1);
            //}
            //ReorderableList.defaultBehaviours.DoRemoveButton(listColors);      
        };

        soundList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = soundList.serializedProperty.GetArrayElementAtIndex(index);

            float x = rect.x;

            EditorGUI.LabelField(
               new Rect(x, rect.y, 40, EditorGUIUtility.singleLineHeight),
               "Tag");

            x += 45;
            EditorGUI.PropertyField(
            new Rect(x, rect.y, (rect.width - 100) / 2, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("tag"), GUIContent.none);

            x += (rect.width - 100) / 2 + 10;
            EditorGUI.LabelField(
              new Rect(x, rect.y, 40, EditorGUIUtility.singleLineHeight),
              "Audio Name");

            x += 45;
            EditorGUI.PropertyField(
            new Rect(x, rect.y, (rect.width - 100) / 2, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("audioName"), GUIContent.none);
        };



    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        this.DrawScript();

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_onTriggerEnter"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_onTriggerExit"));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_onCollisionEnter"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_onCollisionExit"));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("minVelocity"));

        EditorGUILayout.Space();

        sound.playSourceSound = EditorGUILayout.Toggle("Play Source Sound", sound.playSourceSound);

        if (!sound.playSourceSound)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("playOnSelf"));
            if (sound.playOnSelf && sound.GetComponent<SoundComponent>() == null)
            {
                EditorGUILayout.HelpBox("Need a Sound Component to play on self.", MessageType.Error);
            }
            sound.audioNameDefault = EditorGUILayout.TextField("Audio Name Default", sound.audioNameDefault);
            if (soundList != null)
                soundList.DoLayoutList();
        }
        else if (sound.GetComponent<SoundComponent>() == null)
        {
            EditorGUILayout.HelpBox("Need a Sound Component to play on self.", MessageType.Error);
        }





        serializedObject.ApplyModifiedProperties();
    }
}
