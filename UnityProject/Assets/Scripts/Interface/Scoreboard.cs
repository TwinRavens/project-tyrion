﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Scoreboard : MonoBehaviour
{

	public GameManager gameManager;
	public Color[] playerActive = new Color[2], playerDeactivated = new Color[2];
	private Image[] players = new Image[2];
	[System.NonSerialized]
	public TextMeshProUGUI round;
	private TextMeshProUGUI parcial;
	private TextMeshProUGUI[] pontos = new TextMeshProUGUI[2];
	private Animator animator;
	private GameObject[] balls;
	private bool player1 = false;
	private int[] scoreParcial = new int[2];
	private int[] score = new int[2];
	void Start()
	{
		animator = GetComponent<Animator>();
		round = transform.GetChild(0).GetChild(4).GetComponent<TextMeshProUGUI>();
		parcial = transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>();
		players[0] = transform.GetChild(0).GetChild(1).GetComponent<Image>();
		pontos[0] = transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>();
		players[1] = transform.GetChild(0).GetChild(2).GetComponent<Image>();
		pontos[1] = transform.GetChild(0).GetChild(2).GetChild(1).GetComponent<TextMeshProUGUI>();
		Transform divider = transform.GetChild(0).GetChild(3);
		balls = new GameObject[divider.childCount];
		for (int i = 0; i < balls.Length; i++)
			balls[i] = divider.GetChild(i).gameObject;
	}

	void LateUpdate()
	{
		if (player1 != gameManager.teamCounter)
		{
			if (gameManager.hasABolim && gameManager.canLaunch)
			{
				player1 = gameManager.teamCounter;
				StartCoroutine(MudarJogador());
			}
			scoreParcial = gameManager.CurrentScore();
			if (scoreParcial[0] == 1)
				animator.SetBool("player1", true);
			else
				animator.SetBool("player1", false);
			animator.SetInteger("pontuacao", scoreParcial[1]);
			parcial.text = scoreParcial[1].ToString();
			BallsPlayed();
		}
	}

	void BallsPlayed()
	{
		for (int i = 0; i < gameManager.balls.Count; i++)
			if (i < balls.Length)
				balls[i].SetActive(false);
	}
	public void Reset()
	{
		for (int i = 0; i < balls.Length; i++)
			balls[i].SetActive(true);
		StartCoroutine(Bolim());
	}
	public void SetScores()
	{
		for (int i = 0; i < 2; i++)
			pontos[i].text = gameManager.player[i].score.ToString();
	}

	IEnumerator Bolim()
	{
		//Debug.Log("Branqueou!");
		Color[] colors = new Color[2];
		for (int i = 0; i < 2; i++)
			colors[i] = players[i].color;
		float timer = 0f;

		while (timer < 1f)
		{
			timer += Time.deltaTime;
			for (int i = 0; i < 2; i++)
				players[i].color = Color.Lerp(colors[i], Color.white, timer);
			yield return null;
		}
	}

	IEnumerator MudarJogador()
	{
		//Debug.Log("Entrou!");
		Color[] colors = new Color[2];
		for (int i = 0; i < 2; i++)
			colors[i] = players[i].color;
		float timer = 0f;

		while (timer < 1f)
		{
			timer += Time.deltaTime;
			if (player1)
			{
				players[0].color = Color.Lerp(colors[0], playerActive[0], timer);
				players[1].color = Color.Lerp(colors[1], playerDeactivated[1], timer);
			}
			else
			{
				players[0].color = Color.Lerp(colors[0], playerDeactivated[0], timer);
				players[1].color = Color.Lerp(colors[1], playerActive[1], timer);
			}
			yield return null;
		}
	}

	public void EndGame(bool player1Winner)
	{
		for (int i = 0; i < 2; i++)
			players[i].color = Color.black;
		//Debug.Log("Entrou endgame scoreboard!");
		animator.SetBool("winner", true);
		animator.SetBool("player1", player1Winner);
	}
}
