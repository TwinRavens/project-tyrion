﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Ball : MonoBehaviour
{
	/// <summary>
	/// Is this ball a bolim?
	/// </summary>
	public bool bolim;
	/// <summary>
	/// Is this ball moving?
	/// </summary>
	public bool moving;
	/// <summary>
	/// Distance from the ball to the target.
	/// </summary>
	public float distance;

	public bool inGame;

	[System.NonSerialized]
	public int team;

	[System.NonSerialized]
	public Color color;

	private bool canTestMoving = false;
	private TextMeshPro child;
	private Rigidbody rb;

	private void Start()
	{
		rb = GetComponent<Rigidbody>();
		moving = true;
		inGame = false;
		child = transform.GetChild(0).GetComponent<TextMeshPro>();
	}

	public void SetTeam(int team, Color color)
	{
		this.team = team;
		this.color = color;
        MeshRenderer mr = GetComponent<MeshRenderer>();
        mr.material.color = color;
        mr.material.SetColor("_Emission", color);
        //GetComponent<MeshRenderer>().material.DisableKeyword("_EMISSION");
        child = transform.GetChild(0).GetComponent<TextMeshPro>();
		child.color = color;
		child.outlineColor = Color.black;

	}

	public void EnableDistance(bool enable)
	{
		child.text = distance.ToString("F2") + "m";
		child.gameObject.SetActive(enable);
	}

	private void LateUpdate()
	{
		Vector3 horVel = new Vector3(rb.velocity.x, 0, rb.velocity.z);
		if (canTestMoving)
		{
			if (horVel.magnitude < 0.01f)
			{
				moving = false;
				rb.velocity = Vector3.zero;
			}
		}
		else if (horVel.magnitude > 0.001f)
		{
			canTestMoving = moving = true;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("InsideCourse"))
			inGame = true;

	}

	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("InsideCourse"))
			inGame = false;
	}
}
