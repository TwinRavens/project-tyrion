﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EulerRotationArduino : MonoBehaviour
{
    Quaternion rotOffset = Quaternion.identity;
    // Use this for initialization
    void Start()
    {
        God.Events.EventManager.Instance.AddListener<ArduinoReadEvent>(ReadArduino);
        rotOffset = transform.rotation;
    }

    float x, y, z, w, delta;
    ulong count;

    public bool USEYPR = false;
    public bool USEQUATERNION = false;

    void ReadArduino(ArduinoReadEvent data)
    {

        if (USEYPR)
        {
            if (data.read.Length < 3)
                return;

            if (data.read[0] != 'y' || data.read[1] != 'p' || data.read[2] != 'r')
                return;
        }
        else if (USEQUATERNION)
        {
            if (data.read.Length < 4)
                return;

            if (data.read[0] != 'q' || data.read[1] != 'u' || data.read[2] != 'a' || data.read[3] != 't')
                return;
        }
        else
        {
            if (data.read.Length < 5)
                return;

            if (data.read[0] != 'e' || data.read[1] != 'u' || data.read[2] != 'l' || data.read[3] != 'e' || data.read[4] != 'r')
                return;
        }
        string[] s = data.read.Split('\t');


        try
        {
            if (USEQUATERNION)
            {
                x = float.Parse(s[1]);
                y = float.Parse(s[2]);
                z = float.Parse(s[3]);
                w = float.Parse(s[4]);
                count = ulong.Parse(s[5]);
                delta = float.Parse(s[6]);

                transform.rotation = new Quaternion(x/count, z/count, y/count, -w/count);
            }
            else
            {
                x = float.Parse(s[1]);
                y = float.Parse(s[2]);
                z = float.Parse(s[3]);
                delta = float.Parse(s[4]);

                transform.rotation = Quaternion.Euler(new Vector3(x, y, z));
            }

        }
        catch
        {
            Debug.LogError("Couldn't parse " + data.read);
        }




    }
}
