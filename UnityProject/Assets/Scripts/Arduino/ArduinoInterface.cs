﻿
//Unity Includes
using UnityEngine;

//System Includes
using System.Collections;
using System.IO.Ports;
using System;
using God.Events;

public class ArduinoReadEvent : GameEvent
{
    // ACC, QUAT
    public ArduinoReadEvent(string s)
    {
        read = s;
    }
    public string read;
}

public class ArduinoInterface : Singleton<ArduinoInterface>
{
    SerialPort stream;
    EventManager eventManager;
#if UNITY_EDITOR
    public bool debug = true;
#endif
    [Tooltip("Port name with which the SerialPort object will be created.")]
    public string portName = "COM3";

    [Tooltip("Baud rate that the serial device is using to transmit data.")]
    public int baudRate = 115200; // 9600

    Coroutine readingCouroutine = null;
    Coroutine getInformationCoroutine = null;


    [Tooltip("Interval in seconds between gets")]
    public float intervalBetweenGets = 0.4f;

    [SerializeField, InspectorReadOnly]
    private bool m_connected = false;

    public bool connected
    {
        get { return m_connected; }
        set { m_connected = value; }
    }

    void Start()
    {
        eventManager = EventManager.Instance;
        m_connected = false;
    }

    public void ConnectToArduino(Action<string> errorCallback = null)
    {
        if (stream != null)
        {
            DisconnectFromArduino();
        }

        try
        {
            stream = new SerialPort(portName, baudRate) { ReadTimeout = 1 };
        }
        catch (Exception e)
        {
            if (errorCallback != null)
                errorCallback(e.ToString());
            Debug.LogError(e);
            return;
        }

        if (!stream.IsOpen)
        {
            try
            {
                stream.Open();
            }
            catch (Exception e)
            {
                if (errorCallback != null)
                {
                    errorCallback(e.ToString());
                }
                Debug.LogError(e);
                return;
            }
        }

        m_connected = true;
        Debug.Log("Connected to Arduino");

        getInformationCoroutine = StartCoroutine(GetInformationArduino());



        readingCouroutine = StartCoroutine(
               AsynchronousReadFromArduino(
                   CallbackArduinoResponse,     // Callback
                   () => Debug.LogError("Error!")// Error callback
                   )
              );
    }

    public void DisconnectFromArduino()
    {
        m_connected = false;
        if (stream != null)
        {
            stream.Close();
            if (readingCouroutine != null)
                StopCoroutine(readingCouroutine);
            if (getInformationCoroutine != null)
                StopCoroutine(getInformationCoroutine);
            Debug.Log("Disconnected from Arduino");

        }
        stream = null;
        readingCouroutine = null;
        getInformationCoroutine = null;
    }

    private void OnDestroy()
    {
        DisconnectFromArduino();
    }


    private void CallbackArduinoResponse(string s)
    {
#if UNITY_EDITOR
        if (debug)
            Debug.Log(s);
#endif

        eventManager.TriggerEvent(new ArduinoReadEvent(s));

    }

    public bool SendToArduino(string s)
    {
        try
        {
            stream.WriteLine(s);
            stream.BaseStream.Flush();
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            return false;
        }

        /* if (waitingResponse)
         {
             if (readingCouroutine != null) // Stop read coroutine if it exists (????)
                 StopCoroutine(readingCouroutine);

             readingCouroutine = StartCoroutine(
                   AsynchronousReadFromArduino(
                       CallbackArduinoResponse,     // Callback
                       () => Debug.LogError("Error!"), // Error callback
                       10)
                  );
         }*/
    }

    public IEnumerator GetInformationArduino()
    {
        while (SendToArduino("get"))
        {

            yield return new WaitForSeconds(intervalBetweenGets);
        }
        m_connected = false;

    }

    public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity)
    {
        DateTime initialTime = DateTime.Now;
        DateTime nowTime;
        TimeSpan diff = default(TimeSpan);

        string dataString = null;

        do
        {
            try
            {
                dataString = stream.ReadLine();
            }
            catch (TimeoutException)
            {
                dataString = null;
            }

            if (dataString != null)
            {
                callback(dataString);
                yield return null;
            }
            else
                yield return new WaitForSeconds(0.05f);

            nowTime = DateTime.Now;
            diff = nowTime - initialTime;

        } while (diff.Seconds < timeout);

        if (fail != null)
            fail();

        yield return null;
    }

    public IEnumerator AsynchronousReadOnceFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity)
    {
        DateTime initialTime = DateTime.Now;
        DateTime nowTime;
        TimeSpan diff = default(TimeSpan);

        string dataString = null;
        bool failed = true;
        do
        {
            try
            {
                dataString = stream.ReadLine();
            }
            catch (TimeoutException)
            {
                dataString = null;
            }

            if (dataString != null)
            {
                callback(dataString);
                failed = false;
                yield return null;
                break;
            }
            else
                yield return new WaitForSeconds(0.05f);

            nowTime = DateTime.Now;
            diff = nowTime - initialTime;

        } while (diff.Seconds < timeout);

        if (fail != null && failed)
            fail();

        yield return null;
    }
}
