# Project Tyrion

Project Tyrion is a game that uses a arduino with accelerometer as a controller.

# Setup Devlopment Arduino

## Arduino board
 To develop with the arduino first you have to have the propper hardware. We are using the UNO board and a MPU6050 as the accelerometer. 
 This is the design we are using:
 
 ![alt design board](./Documentation/Sketch_bb.jpg)
 
 It's basically the VCC connected to the 5v pin, GND on GND, INT connected to the pin 2, SCL on A5 and SDA on A4. On mega, SCL and SDA have to be pined to 21 and 20, respectively.
 
## Arduino Enviroment
 Firstly you need to download the Arduino IDE on `https://www.arduino.cc/en/Main/Software`.
 Then to develop we use two libraries:
 
### I2CDevLib
 I2CDevLib is a library used to communicate with the accelerometer. You can download it on github `https://github.com/jrowberg/i2cdevlib`.
 
### ArduinoSerialCommand
 ArduinoSerialCommand is a library used to send and receive data from Unity with the serial port.  You can download it on github `https://github.com/scogswell/ArduinoSerialCommand`.
 
### Setup Instructions 
(1) download both libraries

(2) open the library folder of arduino `C:\Program Files (x86)\Arduino\libraries` 

(3) I2CDEV

  (3.1) copy the folder `I2Cdev` located on the folder `Arduino` to the library folder
  
  (3.2) copy the folder `MPU6050` located on the folder `Arduino` to the library folder
  
(4) copy the folder of `ArduinoSerialCommand` to the library folder. 

  (4.1) If the name is not `ArduinoSerialCommand`, change it to this.
  
(5) use AcelerometroCalibration sketch to get offset parameters

(6) use the offset parameters at the main sketch and compile

(7) open unity and run the scene, the follow on-screen calibration steeps
 
# Arduino Sketches

## MainSketch
As the name implies, this is the main sketch, is where the communication with accelerometer and unity is integrated.

## AcelerometroCalibration
Sketch used to manual calibration of the accelerometer.

## UnitySerial
Unity serial test that uses that receive a ping and send a pong.




 
 
 